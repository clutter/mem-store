# Memory Store
In-memory store with CRUD interfaces and TTL


## Installation

```bash
$ npm install @parthar/mem-store --save
```

## Overview
This is a simple in-memory store implementation. The store uses a unique identity for manipulating data. It supports standard CRUD (create, read, update, delete) interfaces via both sync and async callback mechanisms. In addition, the store implementation also supports TTL (time-to-live) which is optional. The store will purge the data if the TTL expires. If the TTL is omited, the data is un-touched.

The store uses data based on data-references provided by caller. Similarly, the data returned is the actual data-reference in the store. So, be thoughtful about modifying the references to data written-to or read-from store.


## Usage

```js
// memory-store setup
var MemoryStore = require("@parthar/mem-store");
var store = new MemoryStore([]); // use instanceof Array for storing data

// create - will return error if duplicate
store.create("id1", val1); // sync
store.create("id1", val1, function (err) { /* do something */ }); // async

// read - will return error if not found
val = store.read("id1"); // val === val1
store.read("id1", function (err, val) { /* !err && val === val1 */ });

// update - will return error if no existing record
store.update("id1", val2); // store.read("id1") === val
store.update("id1", val2, function (err) { /* store.read("id1") === val2 */ }); // async

// upsert - will update (if exists) or create/insert (if not exists)
store.upsert("id1", val2); // store.read("id1") === val
store.upsert("id1", val2, function (err) { /* store.read("id1") === val2 */ }); // async

// delete - will return error if no existing record
store.delete("id1"); // store.read("id1") instanceof Error
store.delete("id1", function (err) { /* err instanceof Error */ }); // async

// ttl
store.create("id1", val1, 50); // optional ttl in milli-sec
store.create("id1", val1, 50, function (err) { /* do something */ }); // optional ttl in milli-sec
setTimeout(function() { /* store.read("id1") instanceof Error */ }, 75);
```
