"use strict";

function getIdFilter(id) {
    return function idFilter(data) {
        return id === data.id;
    };
}

function findElement(store, id) {
    let result = store.filter(getIdFilter(id));

    if (result.length !== 1) { // eslint-disable-line no-magic-numbers
        return new Error("error finding element");
    }

    return result[0]; // eslint-disable-line no-magic-numbers
}

function createElement(store, id, obj, ttl, update) {
    let ele;
    let err;

    ele = findElement(store, id);
    if (update === undefined) { // upsert
        update = !(ele instanceof Error); // eslint-disable-line no-param-reassign
    }
    if (update === ele instanceof Error) { // XOR check
        err = new Error("cannot create/update element");
    } else if (update) {
        ele.data = obj;
        ele.ttl = ttl;
    } else { // create
        store.push({
            "id": id,
            "ttl": ttl ? Date.now() + ttl : undefined,
            "data": obj
        });
    }

    return err;
}

function removeElement(store, id) {
    let idx = store.findIndex(getIdFilter(id));

    if (idx < 0) { // eslint-disable-line no-magic-numbers
        return new Error("error finding element");
    }
    store.splice(idx, 1); // eslint-disable-line no-magic-numbers

    return null;
}

function pruneTTL(store) {
    let now = Date.now();

    store.forEach(function eachEle(ele, idx) {
        if (ele.ttl && ele.ttl <= now) {
            store.splice(idx, 1); // eslint-disable-line no-magic-numbers
        }
    });
}

class MemoryStore {
    constructor(store) {
        if (store instanceof Array) {
            this._store = store;
        } else {
            throw new Error("store needs to be an instanceof Array");
        }
    }
    create(id, obj, ttl, callback) {
        let err;

        pruneTTL(this._store);
        if (typeof ttl === "function" && !callback) { // optional ttl
            callback = ttl; // eslint-disable-line no-param-reassign
            ttl = null; // eslint-disable-line no-param-reassign
        }
        err = createElement(this._store, id, obj, ttl, false);
        if (err instanceof Error) {
            err = new Error("cannot create element with existing id: " + id);
        }

        return callback ? callback(err) : err;
    }
    read(id, callback) {
        let ele;
        let err;

        pruneTTL(this._store);
        ele = findElement(this._store, id);
        if (ele instanceof Error) {
            ele = err = new Error("cannot read element with id: " + id);
        } else {
            err = null;
            ele = ele.data;
        }

        return callback ? callback(err, ele) : ele;
    }
    update(id, obj, ttl, callback) {
        let err;

        pruneTTL(this._store);
        if (typeof ttl === "function" && !callback) { // optional ttl
            callback = ttl; // eslint-disable-line no-param-reassign
            ttl = null; // eslint-disable-line no-param-reassign
        }
        err = createElement(this._store, id, obj, ttl, true);
        if (err instanceof Error) {
            err = new Error("cannot update non-existent element with id: " + id);
        }

        return callback ? callback(err) : err;
    }
    upsert(id, obj, ttl, callback) {
        let err;

        pruneTTL(this._store);
        if (typeof ttl === "function" && !callback) { // optional ttl
            callback = ttl; // eslint-disable-line no-param-reassign
            ttl = null; // eslint-disable-line no-param-reassign
        }
        err = createElement(this._store, id, obj, ttl, undefined);
        if (err instanceof Error) {
            err = new Error("cannot upsert element with id: " + id);
        }

        return callback ? callback(err) : err;
    }
    delete(id, callback) {
        let err;

        pruneTTL(this._store);
        err = removeElement(this._store, id);
        if (err instanceof Error) {
            err = new Error("cannot delete non-existent element with id: " + id);
        }

        return callback ? callback(err) : err;
    }
}

module.exports = MemoryStore;
