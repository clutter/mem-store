/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint  max-nested-callbacks: ["error", 6]*/
/*eslint  no-new: "off"*/

"use strict";

const assert = require("assert");
const MemoryStore = require("../index.js");

// test data
const id1 = "id1";
const id2 = "id2";
const id3 = "id3";
const id4 = "id4";
const data1 = {
    "ying": "yang"
};
const data2 = {
    "ping": "pong"
};

var store;

before(function (done) {
    store = new MemoryStore([]);
    done();
});

describe("Store", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof MemoryStore, "function");
        done();
    });
    it("should throw error when not initialized by array", function (done) {
        assert.throws(function () {
            new MemoryStore(""); // jshint ignore:line
        });
        done();
    });
    it("should create data - sync", function (done) {
        let err = store.create(id1, data1);

        assert.ok(!err);
        done();
    });
    it("should create data - async", function (done) {
        store.create(id2, data2, function (err1) {
            assert.ok(!err1);
            done();
        });
    });
    it("should read data - sync", function (done) {
        let val1 = store.read(id1);

        assert.ok(!(val1 instanceof Error));
        assert.deepStrictEqual(val1, data1);
        done();
    });
    it("should read data - async", function (done) {
        store.read(id2, function (err, val2) {
            assert.ok(!err);
            assert.ok(!(val2 instanceof Error));
            assert.deepStrictEqual(val2, data2);
            done();
        });
    });
    it("should update data - sync", function (done) {
        let err = store.update(id2, data1);
        let val2 = store.read(id2);

        assert.ok(!err);
        assert.ok(val2);
        assert.deepStrictEqual(val2, data1);
        done();
    });
    it("should update data - async", function (done) {
        store.update(id2, data2, function (err) {
            let val2 = store.read(id2);

            assert.ok(!err);
            assert.ok(val2);
            assert.deepStrictEqual(val2, data2);
            done();
        });
    });
    it("should upsert data when present - sync", function (done) {
        let err = store.upsert(id2, data1);
        let val2 = store.read(id2);

        assert.ok(!err);
        assert.ok(val2);
        assert.deepStrictEqual(val2, data1);
        done();
    });
    it("should upsert data when absent - sync", function (done) {
        let err = store.upsert(id3, data1);
        let val3 = store.read(id3);

        assert.ok(!err);
        assert.ok(val3);
        assert.deepStrictEqual(val3, data1);
        done();
    });
    it("should upsert data when present - async", function (done) {
        store.upsert(id2, data2, function (err) {
            let val2 = store.read(id2);

            assert.ok(!err);
            assert.ok(val2);
            assert.deepStrictEqual(val2, data2);
            done();
        });
    });
    it("should upsert data when absent - async", function (done) {
        store.upsert(id4, data2, function (err) {
            let val2 = store.read(id4);

            assert.ok(!err);
            assert.ok(val2);
            assert.deepStrictEqual(val2, data2);
            done();
        });
    });
    it("should delete data - sync", function (done) {
        let err = store.delete(id1);

        assert.ok(!err);
        err = store.read(id1);
        assert.ok(err instanceof Error);
        done();
    });
    it("should delete data - async", function (done) {
        store.delete(id2, function (err) {
            assert.ok(!err);
            assert.ok(store.read(id2) instanceof Error);
            done();
        });
    });
    it("should expire data by TTL - sync", function (done) {
        function checkExpiry() {
            assert.ok(store.read(id1) instanceof Error);
            done();
        }
        assert.ok(!store.create(id1, data1, 50));
        setTimeout(checkExpiry, 75);
    });
    it("should expire data by TTL - async", function (done) {
        function checkExpiry() {
            assert.ok(store.read(id2) instanceof Error);
            done();
        }
        store.create(id2, data2, 50, function (err) {
            assert.ok(!err);
            setTimeout(checkExpiry, 75);
        });
    });
});

after(function (done) {
    done();
});
